# Stepstick Shield

Stepstick Shield for Arduino UNO. The initial purpose for this is to make the Fab Academy machine building week easier. Inspired by [jw4rd Stepper Shield](https://github.com/jw4rd/stepper).

![Hero Shot](images/hero.jpg)

One could avoid the 0 OHM jumper resistors if the connections would be routed to Arduino pins differently, but the intention to do it this way is to keep the original [GRBL library](https://github.com/gnea/grbl) pin layout.

## Make Your Own

The design is optimized to fit on a [50.8mm x 76.2mm FR1 Copper Clad](https://media.digikey.com/pdf/Data%20Sheets/Sparkfun%20PDFs/TOL-14974_Web.pdf). It is recommended that you export the Gerber files or SVG layers yourself accorting to your process of making. It has been tested with [Mods](https://mods.cba.mit.edu/) and [CopperCAM](http://www.galaad.net/coppercam-eng.html).

> **Important:** Please install the [Fab KiCad library](https://gitlab.fabcloud.org/pub/libraries/electronics/kicad) before opening the KiCad project files.

| Components | Stepstick area | LEDs |
|---|---|---|
| [![All parts](images/pcb-layout-all.jpg)](images/pcb-layout-all.jpg) | [![Stepstick](images/pcb-layout-stepstick.jpg)](images/pcb-layout-stepstick.jpg) | [![LEDs](images/pcb-layout-leds.jpg)](images/pcb-layout-leds.jpg) |

## How-To Videos

Please watch the videos below to get a better sense on how to make your board.

- Exporting Gerbers for PCB Milling (to be added)
- [Tool Paths with CopperCAM](https://youtu.be/TvymRjyVZyQ)
- [PCB Milling with SRM-20](https://youtu.be/3z4R8HdXYrg)
- Riveting (to be added)
- Soldering (to be added)

## BOM

Here is a list of parts to be used. Most of the parts can be found in the original [Fab Inventory](http://fab.cba.mit.edu/about/fab/inv.html). This list does not include Arduino and bipolar stepper motor. Prices may vary depending on place and time of yours.

**Components**

| Name | Qty | DigiKey Part Number | Damage |
|---|---|---|---|
| 0 OHM Resistor 1206 | 2 | 311-0.0ERCT-ND | 0.18 EUR |
| 1K OHM Resistor 1206 | 1 | 311-1.00KFRCT-ND | 0.09 EUR |
| 4.99K OHM Resistor 1206 | 1 | 311-4.99KFRCT-ND | 0.09 EUR |
| Orange LED | 2 | 160-1403-1-ND | 0.54 EUR |
| Male Header 2.54mm pitch 40pos | 1 | S1011EC-40-ND | 0.56 EUR |
| Female Header 2.54mm pitch 50pos | 1 | SAM1213-50-ND | 3.80 EUR |
| Bridge Rectifier | 2 | HD01DICT-ND | 0.76 EUR |
| 100uF Capacitor | 1 | PCE3916CT-ND | 0.59 EUR |
| Power Jack Socket 2x5.5mm | 1 | CP-002AHPJCT-ND | 1.12 EUR |
| TMC2208 SilentStepStick | 1 | 1460-1201-ND | 6.24 EUR |
| Screw Terminal 3.5mm pitch | 2 | ED1514-ND | 1.82 EUR |
| **Total** | | | **15.79 EUR** |

**PCB Materials**

| Name | Qty | Link | Damage |
|---|---|---|---|
| Single Sided FR1 Copper Clad | 10 | [SparkFun  TOL-14974](https://www.sparkfun.com/products/14974) | 10 EUR |
| Rivets 1mm inner dia | 1000 | [Reichelt NIETEN 1,0MM](https://www.reichelt.de/hohlnieten-fuer-bel-favorit-1-0-mm-nieten-1-0mm-p33847.html?CCOUNTRY=445&LANGUAGE=de&trstct=pos_2&nbc=1&&r=1) | 18.93 EUR |
| **Total** | | | 28.93 EUR |

> **Warning:** Shipping is not included!
